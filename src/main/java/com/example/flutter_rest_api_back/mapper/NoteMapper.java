package com.example.flutter_rest_api_back.mapper;

import com.example.flutter_rest_api_back.models.NoteDto;
import com.example.flutter_rest_api_back.models.NoteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NoteMapper {
    NoteDto entityToDto(NoteEntity noteEntity);
    NoteEntity dtoToEntity(NoteDto noteDto);
}
