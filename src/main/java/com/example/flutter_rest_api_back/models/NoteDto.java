package com.example.flutter_rest_api_back.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoteDto {
    private String noteId;
    private String noteTitle;
    private String noteContent;
    private LocalDateTime createDateTime;
    private LocalDateTime lastEditDateTime;
}
