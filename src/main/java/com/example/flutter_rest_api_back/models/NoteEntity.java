package com.example.flutter_rest_api_back.models;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class NoteEntity {
    @Id
    private String noteId;
    private String noteTitle;
    private String noteContent;
    private LocalDateTime createDateTime;
    private LocalDateTime lastEditDateTime;
}
