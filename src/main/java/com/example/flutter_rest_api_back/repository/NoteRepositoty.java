package com.example.flutter_rest_api_back.repository;

import com.example.flutter_rest_api_back.models.NoteEntity;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface NoteRepositoty extends JpaRepository<NoteEntity, String> {
}
