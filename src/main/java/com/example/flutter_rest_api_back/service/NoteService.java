package com.example.flutter_rest_api_back.service;

import com.example.flutter_rest_api_back.models.NoteDto;
import java.util.List;

public interface NoteService {
  List<NoteDto> getAllNotes();

  NoteDto addNote(NoteDto noteDto);

  NoteDto getNoteById(String noteId);

  NoteDto updateNote(String noteId, NoteDto noteDto);

  void deleteNote(String noteId);
}
