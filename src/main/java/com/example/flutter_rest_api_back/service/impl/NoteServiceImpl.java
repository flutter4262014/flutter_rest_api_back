package com.example.flutter_rest_api_back.service.impl;

import com.example.flutter_rest_api_back.exception.NoteNotFoundException;
import com.example.flutter_rest_api_back.mapper.NoteMapper;
import com.example.flutter_rest_api_back.models.NoteDto;
import com.example.flutter_rest_api_back.models.NoteEntity;
import com.example.flutter_rest_api_back.repository.NoteRepositoty;
import com.example.flutter_rest_api_back.service.NoteService;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NoteServiceImpl implements NoteService {

  private final NoteRepositoty noteRepositoty;
  private final NoteMapper noteMapper;

  @Override
  public List<NoteDto> getAllNotes() {
    return noteRepositoty.findAll().stream()
        .map(noteMapper::entityToDto)
        .collect(Collectors.toList());
  }

  @Override
  public NoteDto addNote(NoteDto noteDto) {
    noteDto.setNoteId(UUID.randomUUID().toString());
    noteDto.setCreateDateTime(LocalDateTime.now());
    noteDto.setLastEditDateTime(LocalDateTime.now());

    return noteMapper.entityToDto(noteRepositoty.save(noteMapper.dtoToEntity(noteDto)));
  }

  @Override
  public NoteDto getNoteById(String noteId) {
    return noteMapper.entityToDto(
        noteRepositoty
            .findById(noteId)
            .orElseThrow(
                () -> new NoteNotFoundException("Note with id " + noteId + " not found.")));
  }

  @Override
  public NoteDto updateNote(String noteId, NoteDto noteDto) {
    Optional<NoteEntity> noteEntity = noteRepositoty.findById(noteId);

    return noteMapper.entityToDto(
        noteEntity
            .map(
                note -> {
                  note.setNoteTitle(noteDto.getNoteTitle());
                  note.setNoteContent(noteDto.getNoteContent());
                  return noteRepositoty.save(note);
                })
            .orElseThrow(
                () ->
                    new NoteNotFoundException(
                        String.format("Note with id %s not found.", noteId))));
  }

  @Override
  public void deleteNote(String noteId) {
    Optional<NoteEntity> noteEntity = noteRepositoty.findById(noteId);
    noteEntity.ifPresentOrElse(
        note -> noteRepositoty.deleteById(noteId),
        () -> {
          throw new NoteNotFoundException(String.format("Note with id %s not found.", noteId));
        });
  }
}
