package com.example.flutter_rest_api_back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FlutterRestApiBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(FlutterRestApiBackApplication.class, args);
	}

}
