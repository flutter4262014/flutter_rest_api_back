package com.example.flutter_rest_api_back.controller;

import com.example.flutter_rest_api_back.models.NoteDto;
import com.example.flutter_rest_api_back.service.NoteService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/flutter/rest")
@RequiredArgsConstructor
public class NoteController {
  private final NoteService noteService;

  @GetMapping("/notes")
  public ResponseEntity<List<NoteDto>> getAllNotes() {
    return ResponseEntity.ok(noteService.getAllNotes());
  }

  @PostMapping("/add")
  public ResponseEntity<NoteDto> addNote(@RequestBody NoteDto noteDto) {
    return ResponseEntity.ok(noteService.addNote(noteDto));
  }

  @GetMapping("/find/{noteId}")
  public ResponseEntity<NoteDto> getNoteById(@PathVariable("noteId") String noteId) {
    return new ResponseEntity<>(noteService.getNoteById(noteId), HttpStatus.OK);
  }

  @PutMapping("/update/{noteId}")
  public ResponseEntity<NoteDto> updateNote(
      @PathVariable("noteId") String noteId, @RequestBody NoteDto noteDto) {
    return new ResponseEntity<>(noteService.updateNote(noteId, noteDto), HttpStatus.OK);
  }

  @DeleteMapping("/delete/{noteId}")
  public ResponseEntity<Void> deleteNote(@PathVariable("noteId") String noteId) {
    noteService.deleteNote(noteId);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
